const express=require('express')
const cors=require('cors')
const { response } = require('express')

const app=express()
app.use(cors('*'))
app.use(express.json())

app.get('/', (request ,response)=>
{
    response.send('up here again')
})

app.listen(3000,'0.0.0.0', ()=>{
    console.log('server up and running on 3000')
})